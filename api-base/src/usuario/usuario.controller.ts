import { Controller, Delete, Get } from '@nestjs/common';

@Controller('usuario') //path principal http:localhost:3000/usuario
export class UsuarioController {

    @Get('obtener-usuarios') //path principal http:localhost:3000/usuario/obtener-usuarios
    obtenerUsuarios(){
        return{
            mensaje: 'Todos los usuarios'
        }
    }

    @Delete('eliminar-usuarios') //path principal http:localhost:3000/usuario/eliminar-usuarios
    eliminarUsuario(){
        return{
            mensaje: 'Todos los usuarios'
        }
    }

}
